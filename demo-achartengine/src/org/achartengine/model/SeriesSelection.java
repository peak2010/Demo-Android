/**
 * Copyright (C) 2009 - 2013 SC 4ViewSoft SRL
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.achartengine.model;

import android.graphics.Canvas;
import android.graphics.Paint;

public class SeriesSelection {
  private int mSeriesIndex;

  private int mPointIndex;

  private double mXValue;

  private double mValue;
  
  private double screenX;
  private double screenY;
  
  public SeriesSelection(int seriesIndex, int pointIndex, double xValue, double value) {
    mSeriesIndex = seriesIndex;
    mPointIndex = pointIndex;
    mXValue = xValue;
    mValue = value;
  }
  
  public SeriesSelection(int seriesIndex, int pointIndex, double xValue, double value, double x, double y) {
    mSeriesIndex = seriesIndex;
    mPointIndex = pointIndex;
    mXValue = xValue;
    mValue = value;
    screenX = x;
    screenY = y;
  }

  public int getSeriesIndex() {
    return mSeriesIndex;
  }

  public int getPointIndex() {
    return mPointIndex;
  }

  public double getXValue() {
    return mXValue;
  }

  public double getValue() {
    return mValue;
  }

  public double getScreenX() {
    return screenX;
  }

  public void setScreenX(double screenX) {
    this.screenX = screenX;
  }

  public double getScreenY() {
    return screenY;
  }

  public void setScreenY(double screenY) {
    this.screenY = screenY;
  }
  
  
}