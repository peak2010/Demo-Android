package org.achartengine.chartdemo.demo;

import java.util.ArrayList;
import java.util.List;

import org.achartengine.chart.PointStyle;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class SalesReportActivity extends Activity {

  private CustomChart customChart;
  private RelativeLayout rlMain;
  private LinearLayout ll_main;
  private Button btToday;
  private Button btYesterday;
  private Button btWeek;
  private Button btMonth;
  private Button btYear;

  private static Integer DATA_TODAY = 100;
  private static Integer DATA_YESTERDAY = 101;
  private static Integer DATA_WEEK = 102;
  private static Integer DATA_MONTH = 103;
  private static Integer DATA_YEAR = 104;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.sale_report);
    initUI();
  }

  /**
   * Initialize the UI.
   */
  public void initUI() {
    rlMain = (RelativeLayout) this.findViewById(R.id.rl_main);
    ll_main = (LinearLayout) this.findViewById(R.id.ll_main);
    btToday = (Button) this.findViewById(R.id.bt1);
    btYesterday = (Button) this.findViewById(R.id.bt2);
    btWeek = (Button) this.findViewById(R.id.bt3);
    btMonth = (Button) this.findViewById(R.id.bt4);
    btYear = (Button) this.findViewById(R.id.bt5);

    btToday.setOnClickListener(clickListener);
    btYesterday.setOnClickListener(clickListener);
    btWeek.setOnClickListener(clickListener);
    btMonth.setOnClickListener(clickListener);
    btYear.setOnClickListener(clickListener);

    // Set chart view.
    setChartView(DATA_TODAY);
  }

  // Create clickListener
  private OnClickListener clickListener = new OnClickListener() {

    @Override
    public void onClick(View v) {
      if (null != customChart.pw && customChart.pw.isShowing()) {
        customChart.pw.dismiss();
        customChart.pw = null;
      }
      if (null != customChart.point && customChart.point.isShowing()) {
        customChart.point.dismiss();
        customChart.point = null;
      }
      switch (v.getId()) {
      case R.id.bt1:
        setChartView(DATA_TODAY);
        break;
      case R.id.bt2:
        setChartView(DATA_YESTERDAY);
        break;
      case R.id.bt3:
        setChartView(DATA_WEEK);
        break;
      case R.id.bt4:
        setChartView(DATA_MONTH);
        break;
      case R.id.bt5:
        setChartView(DATA_YEAR);
        break;
      }
    }
  };

  /**
   * Set chart view.
   * 
   * @param Integer chartType.
   * 
   * */
  public void setChartView(Integer chartType) {
    ll_main.removeAllViews();
    customChart = new CustomChart(setCustomChartEntity(chartType), rlMain);
    View view = customChart.setCustomView(SalesReportActivity.this);
    ll_main.addView(view);
  }

  /**
   * 
   * Initialize the chart entity
   * 
   * @param Integer chartType.
   * 
   * */
  public CustomChartEntity setEntity(CustomChartEntity entity, List<Double> valueList,
      List<Double> xList, List<String> xTextList) {

    int valueSize = valueList.size();
    int xSize = xList.size();
    int xTextSize = xTextList.size();
    if (valueSize == xSize && xSize == xTextSize) {

      double[] value = new double[valueList.size()];
      double[] x = new double[xList.size()];
      String[] xText = new String[xTextList.size()];
      for (int i = 0; i < valueSize; i++) {
        value[i] = valueList.get(i);
        x[i] = xList.get(i);
        xText[i] = xTextList.get(i);
      }

      entity.values.add(value);
      entity.x.add(x);
      entity.xText = xText;

    } else {
      Log.i("Milo", "The data not format!");
    }
    return entity;
  }

  /**
   * 
   * Set the data for chart entity
   * 
   * @param Integer chartType. 
   * 
   * */
  public CustomChartEntity setCustomChartEntity(Integer dataType) {

    CustomChartEntity entity = new CustomChartEntity();
    List<double[]> values = new ArrayList<double[]>();
    List<double[]> x = new ArrayList<double[]>();

    if (DATA_TODAY == dataType) {
      entity.titles = new String[] { "Today" };

      // Set data for the chart
      values
          .add(new double[] { 100, 111.0, 253, 1.00, 20.4, 24.4, 26.4, 253, 159, 20.4, 24.4, 1.00 });
      for (int i = 0; i < entity.titles.length; i++) {
        x.add(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
      }
      entity.values = values;
      entity.x = x;
      entity.xText = new String[] { "2AM", "4AM", "6AM", "8AM", "10AM", "12PM", "2PM", "4PM",
          "6PM", "8PM", "10PM", "12AM" };

    } else if (DATA_YESTERDAY == dataType) {
      entity.titles = new String[] { "Yesterday" };

      values.add(new double[] { 212, 10000.0, 987, 34, 91, 34, 26.4, 253, 159, 20.4, 24.4, 45 });
      for (int i = 0; i < entity.titles.length; i++) {
        x.add(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
      }
      entity.values = values;
      entity.x = x;
      entity.xText = new String[] { "2AM", "4AM", "6AM", "8AM", "10AM", "12PM", "2PM", "4PM",
          "6PM", "8PM", "10PM", "12AM" };

    } else if (DATA_WEEK == dataType) {
      entity.titles = new String[] { "Week" };

      values.add(new double[] { 100, 200.0, 253, 0.00, 20.4, 24.4, 26.4 });
      for (int i = 0; i < entity.titles.length; i++) {
        x.add(new double[] { 1, 2, 3, 4, 5, 6, 7 });
      }
      entity.values = values;
      entity.x = x;
      entity.xText = new String[] { "周一", "周二", "周三", "周四", "周五", "周六", "周日" };

    } else if (DATA_MONTH == dataType) {
      entity.titles = new String[] { "Month" };

      values.add(new double[] { 100, 200.0, 253, 0.00 });
      for (int i = 0; i < entity.titles.length; i++) {
        x.add(new double[] { 1, 2, 3, 4 });
      }
      entity.values = values;
      entity.x = x;
      entity.xText = new String[] { "Week1", "Week2", "Week3", "Week4" };

    } else if (DATA_YEAR == dataType) {
      entity.titles = new String[] { "Year" };

      values.add(new double[] { 10, 206.0, 453, 0.00, 120.4, 124.4, 326.4, 153, 59, 20.4, 124.4,
          23.00 });
      for (int i = 0; i < entity.titles.length; i++) {
        x.add(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
      }
      entity.values = values;
      entity.x = x;
      entity.xText = new String[] { "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月",
          "十一月", "十二月" };
    }

    entity.chartName = "Chart Line";
    entity.chartDesc = "Chart line demo ";

    entity.colors = new int[] { Color.WHITE };
    entity.styles = new PointStyle[] { PointStyle.CUSTOM };
    return entity;
  }
}
