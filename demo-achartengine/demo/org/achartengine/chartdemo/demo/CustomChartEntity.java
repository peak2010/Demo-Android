package org.achartengine.chartdemo.demo;

import java.util.List;

import org.achartengine.chart.PointStyle;


public class CustomChartEntity {


  public String chartName;
  public String chartDesc;
  
  public String[] titles;
  public List<double []> values;
  public List<double []> x;
  public int[] colors;
  public PointStyle[] styles;
  public String[] xText;

}
