package org.achartengine.chartdemo.demo;

import org.achartengine.GraphicalView;
import org.achartengine.chart.LineChart;
import org.achartengine.chartdemo.demo.R;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

public class CustomChart extends AbstractDemoChart {

  public CustomChartEntity entity;
  public ViewGroup rootView;
  private View popUpLayout;
  private View pointView;
  public PopupWindow pw = null;
  public PopupWindow point = null;
  private CustomLineChart lineChart = null;

  public CustomChart(CustomChartEntity customChartEntity, ViewGroup root) {
    this.entity = customChartEntity;
    this.rootView = root;
  }

  /**
   * Set the custom chart view.
   * 
   * @param Context context.
   * 
   * */
  public View setCustomView(final Context context) {

    final XYMultipleSeriesRenderer renderer = buildRenderer(entity.colors, entity.styles);
    final int length = renderer.getSeriesRendererCount();
    for (int i = 0; i < length; i++) {
      final XYSeriesRenderer xySeriesRenderer = (XYSeriesRenderer) renderer.getSeriesRendererAt(i);
      xySeriesRenderer.setFillPoints(true);
    }
    setChartSettings(renderer, "", "", "", 0, entity.values.get(0).length + 1,
        getMin(entity.values.get(0)) - 1, getMax(entity.values.get(0)) + 1, Color.LTGRAY,
        Color.LTGRAY);

    renderer.setPointSize(50);
    renderer.setBackgroundColor(Color.TRANSPARENT);
    renderer.setShowAxes(true);
    renderer.setShowGrid(false);

    renderer.setShowTickMarks(false);
    renderer.setYLabelsAlign(Align.RIGHT);
    renderer.setYLabelsColor(0, Color.BLACK);
    renderer.setLabelsTextSize(20);

    renderer.setYLabels(0);
    renderer.setXLabels(0);

    renderer.setShowLegend(false);

    int xLength = entity.x.get(0).length;
    for (int i = 0; i < xLength; i++) {
      renderer.setXLabelsColor(Color.BLACK);
      renderer.addXTextLabel(i + 1, entity.xText[i]);
    }

    // Set Y-line values
    renderer.setShowLabels(true);
    if (getMax(entity.values.get(0)) == 0) {
        // For the case all data are 0.00.
        renderer.addYTextLabel(0, "0.00");
        renderer.addYTextLabel(1, getMax(entity.values.get(0)) / 2 + "");
        renderer.addYTextLabel(2, getMax(entity.values.get(0)) + "");

        renderer.setYAxisMin(0.00);
        renderer.setYAxisMax(2);
    } else {
      renderer.addYTextLabel(getMin(entity.values.get(0)), getMin(entity.values.get(0)) + "");
      renderer.addYTextLabel(getMax(entity.values.get(0)) / 2, getMax(entity.values.get(0)) / 2
          + "");
      renderer.addYTextLabel(getMax(entity.values.get(0)), getMax(entity.values.get(0)) + "");
      renderer.setYAxisMin(getMin(entity.values.get(0)));
      renderer.setYAxisMax(getMax(entity.values.get(0)));
    }


    renderer.setXLabelsAlign(Align.CENTER);
    renderer.setXLabelsPadding(1000);
    renderer.setMargins(new int[] { 50, 0, 50, 50 });
    renderer.setClickEnabled(true);
    renderer.setPanEnabled(false, false);
    renderer.setZoomEnabled(false, false);

    lineChart = new CustomLineChart(buildDataset(entity.titles, entity.x, entity.values), renderer,
        context);
    final GraphicalView view = new GraphicalView(context, lineChart);

    LayoutInflater inflater = ((Activity) context).getLayoutInflater();
    popUpLayout = inflater.inflate(R.layout.sale_report_popup, null);

    pointView = inflater.inflate(R.layout.sale_report_point, null);

    view.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(final View v) {

        int[] location = new int[2];
        v.getLocationInWindow(location);

        final SeriesSelection seriesSelection = view.getCurrentSeriesAndPoint();
        if (seriesSelection == null) {

        } else {
          if (pw != null && pw.isShowing()) {
            pw.dismiss();
          }
          if (point != null && point.isShowing()) {
            point.dismiss();
          }

          updatePointStyle(seriesSelection);
          initPopUp(seriesSelection);
        }
      }
    });
    return view;
  }

  /**
   * Initialize the pop up window
   * 
   * @param SeriesSelection seriesSelection.
   * */
  public void initPopUp(SeriesSelection seriesSelection) {

    popUpLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
        LayoutParams.WRAP_CONTENT));
    popUpLayout.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

    RelativeLayout rl = (RelativeLayout) popUpLayout.findViewById(R.id.rl_window);
    TextView tv1 = (TextView) popUpLayout.findViewById(R.id.tv1);
    TextView tv2 = (TextView) popUpLayout.findViewById(R.id.tv2);

    tv1.setText("Count:10");
    tv2.setText("$" + seriesSelection.getValue());

    popUpLayout.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    pw = new PopupWindow(popUpLayout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, false);

    if (seriesSelection.getValue() > getMax(entity.values.get(0)) * 2 / 3) {
      rl.setBackgroundResource(R.drawable.popup2);
      pw.showAtLocation(rootView, Gravity.NO_GRAVITY, (int) seriesSelection.getScreenX()
          - pw.getContentView().getMeasuredWidth() / 2, (int) seriesSelection.getScreenY()
          + pw.getContentView().getMeasuredHeight() * 7 / 5);
    } else {
      rl.setBackgroundResource(R.drawable.popup1);
      pw.showAtLocation(rootView, Gravity.NO_GRAVITY, (int) seriesSelection.getScreenX()
          - pw.getContentView().getMeasuredWidth() / 2, (int) seriesSelection.getScreenY()
          + pw.getContentView().getMeasuredHeight() * 1 / 5);
    }

  }

  /**
   * Update the point style.
   * 
   * @param SeriesSelection seriesSelection.
   * */
  public void updatePointStyle(SeriesSelection seriesSelection) {

    pointView
        .setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    pointView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

    pointView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    // Use Popup
    point = new PopupWindow(pointView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, false);

    point.showAtLocation(rootView, Gravity.NO_GRAVITY, (int) seriesSelection.getScreenX()
        - point.getContentView().getMeasuredWidth() / 2, (int) seriesSelection.getScreenY()
        + point.getContentView().getMeasuredHeight() * 14 / 4);

  }

  // Custom line chart
  class CustomLineChart extends LineChart {

    public CustomLineChart(final XYMultipleSeriesDataset dataset,
        final XYMultipleSeriesRenderer renderer, Context context) {
      super(dataset, renderer, context);
    }

    private static final long serialVersionUID = 1L;

    /*
     * Set background color.
     */
    @Override
    protected void drawBackground(final DefaultRenderer renderer, final Canvas canvas, final int x,
        final int y, final int width, final int height, final Paint paint, final boolean newColor,
        int color) {
      color = Color.TRANSPARENT;
      super.drawBackground(renderer, canvas, x, y, width, height, paint, newColor, color);
    }

  }

  /**
   * 
   * Get the minimum value from the array.
   * 
   * @param double[] data.
   * 
   * @return the minimum value.
   * 
   * */
  public double getMin(final double[] data) {
    double minData = 0;
    for (final double a : data) {
      if (a == 0) {
        return a;
      }
      minData = (minData == 0) ? a : minData;
      minData = (minData < a) ? minData : a;
    }
    return minData;
  }

  /**
   * 
   * Get the max value from the array.
   * 
   * @param double[] data.
   * 
   * @return the max value.
   * 
   * */
  public double getMax(final double[] data) {
    double maxData = 0;
    for (final double a : data) {
      maxData = (maxData < a) ? a : maxData;
    }
    return maxData;
  }

  /**
   * Returns the chart name.
   * 
   * @return the chart name
   */
  @Override
  public String getName() {
    return entity.chartName;
  }

  /**
   * Returns the chart description.
   * 
   * @return the chart description
   */
  @Override
  public String getDesc() {
    return entity.chartDesc;
  }

  @Override
  public Intent execute(Context context) {
    return null;
  }

}
