package com.milo.oom.demo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/*
 * Test OOM issue and show how to use DDMS
 */
public class MainActivity extends Activity {

	private Boolean addTag = true;
	private Button btLeft;
	private Button btRight;

	private List<OomObject> oomObjs = new ArrayList<OomObject>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btLeft = (Button) this.findViewById(R.id.bt_add);
		btRight = (Button) this.findViewById(R.id.bt_stop);

		btLeft.setOnClickListener(clickListener);
		btRight.setOnClickListener(clickListener);
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.bt_add:
				addTag = true;
				newOomObject();
				break;
			case R.id.bt_stop:
				addTag = false;
				break;
			}

		}
	};

	public void newOomObject() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Resources res = getResources();
				while (addTag) {
					try {
						Thread.sleep(2);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					OomObject oomObj = new OomObject();
					oomObj.objectId = System.currentTimeMillis();
					Bitmap bitmap = BitmapFactory.decodeResource(res,
							R.drawable.ic_launcher);
					oomObj.bitmap = bitmap;
					Log.i("milo", "add " + oomObj.objectId);
					// Reference to the Activity
					oomObjs.add(oomObj);
				}
			}

		}).start();
	}

}
