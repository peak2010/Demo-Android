
package com.augeek.iweather.manager;

import com.augeek.iweather.common.Callback;
import com.augeek.iweather.common.Error;
import com.augeek.iweather.model.ForecastRealTimeResult;
import com.augeek.iweather.model.ForecastSummaryResult;

public interface WeatherService {

    public void getForecast(String location, int days,
 final Callback <ForecastSummaryResult, Error> callback);

    public void getRealTimeWeather(String location,
            final Callback <ForecastRealTimeResult, Error> callback);
}
