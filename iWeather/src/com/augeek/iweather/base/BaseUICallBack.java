/*
 * Copyright 2014 Augmentum Inc. All rights reserved.
 */
package com.augeek.iweather.base;


public abstract class BaseUICallBack<T, K, G> {

    protected void onPostExecute(G result) {

    }

    protected void onPreExecute() {

    }

    protected void onProgressUpdate(K... values) {

    }
}
