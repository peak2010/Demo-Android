/*
 * Copyright 2014 Augmentum Inc. All rights reserved.
 */
package com.augeek.iweather;

public class Constants {

    public static final boolean IS_DEBUG = true;
    
    //Crittercism App ID
    public static final String CRITTERCISM_APP_ID = "53b55359b573f1142d000005";
}
