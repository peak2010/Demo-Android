package com.augeek.iweather.model;

public class ForecastRealTimeResult {

	public double temperature;
	public int humidity;

	public String reportTimeString;

}
