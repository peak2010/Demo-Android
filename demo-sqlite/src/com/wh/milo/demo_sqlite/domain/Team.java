package com.wh.milo.demo_sqlite.domain;

public class Team {

	public final static String TEAM_ID = "team_id";
	public final static String TEAM_NAME = "team_name";
	public final static String TEAM_DESCRIPTION = "team_description";

	public Integer id;
	public String name;
	public String decription;

}
