package com.wh.milo.demo_sqlite.domain;

public class Person {

	public final static String PERSON_ID = "person_id";
	public final static String TEAM_ID = "team_id";
	public final static String PERSON_NAME = "person_name";
	public final static String PERSON_AGE = "person_age";

	public Integer id;
	public Integer teamId;
	public String name;
	public Integer age;
}
