package com.wh.milo.demo_sqlite;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.wh.milo.demo_sqlite.db.DBManager;
import com.wh.milo.demo_sqlite.domain.Person;

public class MainActivity extends Activity {

	private Button btAdd;
	private Button btRemove;
	private TextView tvMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btAdd = (Button) this.findViewById(R.id.bt_add);
		btRemove = (Button) this.findViewById(R.id.bt_remove);
		tvMessage = (TextView) this.findViewById(R.id.tv_message);

		btAdd.setOnClickListener(clickListener);
		btRemove.setOnClickListener(clickListener);
	}

	private OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.bt_add:
				Log.i(Contants.TAG, "Click Add");
				testAdd();
				break;
			case R.id.bt_remove:
				Log.i(Contants.TAG, "Click Query");
				testQuery();
				break;
			}

		}
	};

	public Thread getAddThread(final List<Person> persons) {
		return new Thread(new Runnable() {

			@Override
			public void run() {
				
				final String threadName = Thread.currentThread().getName();
				DBManager manager = DBManager
						.getSingleManager(getApplicationContext());
				
//				final int num = manager.removePersons();
//				runOnUiThread(new Runnable() {
//
//					@Override
//					public void run() {
//						tvMessage.setText(threadName + " Remove "
//								+ num + " persons from DB!");
//					}
//
//				});
				
				Log.i(Contants.TAG, "Add : " + Thread.currentThread().getName() + " : " + System.currentTimeMillis());

				manager.addPerson(persons);

				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						tvMessage.setText(threadName + " Add " + persons.size()
								+ " person into DB!");
					}

				});
			}

		});
	}

	public Thread getQueryThread() {
		return new Thread(new Runnable() {

			@Override
			public void run() {
				DBManager manager = DBManager
						.getSingleManager(getApplicationContext());
				Log.i(Contants.TAG,
						"Query : " + Thread.currentThread().getName() + " : "
								+ System.currentTimeMillis());
				//manager.dropTablePerson();
				//manager.closeDatabase();
				final List<Person> persons = manager.getPersons();
				final String threadName = Thread.currentThread().getName();
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						tvMessage.setText(threadName + " Query "
								+ persons.size() + " persons from DB!");
					}

				});
				
			}

		});
	}

	public void testAdd() {
		final List<Person> persons = new ArrayList<Person>();
		for (int i = 0; i < 100; i++) {
			Person person = new Person();
			person.name = "milo" + i;
			person.age = i;
			person.teamId = 1;
			persons.add(person);
		}

		for (int i = 0; i < 100; i++) {
			getAddThread(persons).start();
		}

	}

	public void testQuery() {
		for (int i = 0; i < 10; i++) {
			getQueryThread().start();
		}
	}

}
