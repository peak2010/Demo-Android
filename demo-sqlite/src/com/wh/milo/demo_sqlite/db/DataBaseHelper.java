package com.wh.milo.demo_sqlite.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.wh.milo.demo_sqlite.Contants;
import com.wh.milo.demo_sqlite.domain.Person;
import com.wh.milo.demo_sqlite.domain.Team;

public class DataBaseHelper extends SQLiteOpenHelper {

	// 数据库版本号
	private static final int DATABASE_VERSION = 2;
	// 数据库名
	private static final String DATABASE_NAME = "demo1.db";

	// 数据表名，一个数据库中可以有多个表（虽然本例中只建立了一个表）
	public static final String TABLE_TEAM = "t_team";
	public static final String TABLE_PERSON = "t_person";

	public DataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	public DataBaseHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		createTables(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEAM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSON);
		createTables(db);
	}

	public void createTables(SQLiteDatabase db) {
		Log.i(Contants.TAG, "DatabaseHelper onCreate");

		// 构建创建表的SQL语句（可以从SQLite Expert工具的DDL粘贴过来加进StringBuffer中）
		StringBuffer personSql = new StringBuffer();

		personSql.append("CREATE TABLE [" + TABLE_PERSON + "] (");
		personSql.append("[" + Person.PERSON_ID
				+ "] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ");
		personSql.append("[" + Person.TEAM_ID + "] INTEGER,");
		personSql.append("[" + Person.PERSON_NAME + "] TEXT,");
		personSql.append("[" + Person.PERSON_AGE + "] INTEGER )");
		// 执行创建表的SQL语句
		db.execSQL(personSql.toString());

		StringBuffer teamSql = new StringBuffer();
		teamSql.append("CREATE TABLE [" + TABLE_TEAM + "] (");
		teamSql.append("[" + Team.TEAM_ID
				+ "] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, ");
		teamSql.append("[" + Team.TEAM_NAME + "] TEXT,");
		teamSql.append("[" + Team.TEAM_DESCRIPTION + "] TEXT)");
		db.execSQL(teamSql.toString());
	}

}
