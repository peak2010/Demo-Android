package com.wh.milo.demo_sqlite.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.wh.milo.demo_sqlite.Contants;
import com.wh.milo.demo_sqlite.domain.Person;
import com.wh.milo.demo_sqlite.domain.Team;

public class DBManager {
	private SQLiteDatabase db;

	private static DBManager dbManager;

	private DBManager(Context context) {
		Log.i(Contants.TAG, "DBManager --> Constructor");
		DataBaseHelper helper = new DataBaseHelper(context);
		db = helper.getWritableDatabase();
	}

	public void closeDatabase() {
		if (db != null) {
			db.close();
		}
	}

	public synchronized static DBManager getSingleManager(Context context) {
		if (dbManager == null) {
			dbManager = new DBManager(context);
		}
		return dbManager;
	}

	public void addPerson(List<Person> persons) {
		Log.i(Contants.TAG, "DBManager --> add person");
		// 采用事务处理，确保数据完整性
		db.beginTransaction(); // 开始事务
		try {
			for (Person person : persons) {
				final ContentValues cv = new ContentValues();
				cv.put(Person.PERSON_NAME, person.name);
				cv.put(Person.PERSON_AGE, person.age);
				cv.put(Person.TEAM_ID, person.teamId);
				db.insert(DataBaseHelper.TABLE_PERSON, null, cv);
			}
			db.setTransactionSuccessful(); // 设置事务成功完成
		} finally {
			db.endTransaction(); // 结束事务
		}
	}

	public List<Person> getPersons() {
		List<Person> persons = new ArrayList<Person>();
		Cursor cur = db.rawQuery(
				"select * from " + DataBaseHelper.TABLE_PERSON, null);
		if (cur.moveToFirst()) {
			do {
				Person person = new Person();
				person.id = cur.getInt(cur.getColumnIndex(Person.PERSON_ID));
				person.name = cur.getString(cur
						.getColumnIndex(Person.PERSON_NAME));
				person.teamId = cur.getInt(cur.getColumnIndex(Person.TEAM_ID));
				person.age = cur.getInt(cur.getColumnIndex(Person.PERSON_AGE));

				persons.add(person);
			} while (cur.moveToNext());
		}
		if (cur != null) {
			cur.close();
		}
		return persons;
	}

	public int removePersons() {
		// db.rawQuery("DELETE * from "+ DataBaseHelper.TABLE_PERSON, null);
		return db.delete(DataBaseHelper.TABLE_PERSON, null, null);
	}

	public void dropTablePerson() {
		db.execSQL("DROP TABLE IF EXISTS " + DataBaseHelper.TABLE_PERSON);
	}

	// TODO
	public boolean addTeam(Team team) {
		return false;
	}

}
